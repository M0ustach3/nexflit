# Nexflit - the new watching website
### _This is the final GLPOO project in the ESIEA, Semester 2 made by the best developers of the silicon valley_
![Banner](./toudoum.jpg)

TODO


#### Bonuses that we have done : 
- Used GitLab to organize our project
- Multiple branches (master, develop,bugfix and feature branches)
- Added permissions on the different branches to avoid pushing directly to it
- Used tickets and boards to organize the tasks
- Added GitLab Webhook to our Discord server to have the issues directly in it
- Added CI configuration to test our code automatically at each merge request
- Added Docker support

## Authors

* **Pablo Bondia-Luttiau** - alias *El pablito* 
* **Lennaïg Charron** - alias *Léanig' chat rond*
* **Théophile Hamelin** - alias *Le défonceur*
* **Samuel Lablée** - alias *Ok boomer*
* **Michaël Ghesquière** - alias *Tom Nook*


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Before running our project, you have to make sure you have Python installed in your system.
Our Python development version was Python 3.8

If you want to use our Docker container, you'll have to install Docker first. If you're on MacOS,
you'll be fine, If you are on Windows, you will __NEED__ to have a Windows 10 Home or Pro.

### Installing

First of all, create a ``.env`` file at the root of the project. Fill it with these 2 environment variables :
```dotenv
DATABASE_ENGINE=sqlite
DATABASE_FILE_NAME=thisIsAGoodFileNameForADatabase.bd
```

Then, install the requirements of the project :

```shell script
pip install -r requirements.txt
```

Now you can start the development server : 

```shell script
python main.py
```

This will run a server on your [http://localhost:5000](http://localhost:5000).

You can now test the API with Postman or any HTTP-capable software : 

```shell script
wget http://localhost:5000/films
```

## Running the tests

To run the tests locally, use this command to test all of the packages : 

```shell script
python -m unittest
```

This will discover any test in the project folder. Be sure to write your tests in the ``test/`` folder,
this will be way easier that way.

## Coding style tests

Flake8 and pylint are used to check the code styling in this project.

### Pylint
For pylint, run this command and check the output :

```shell script
python -m pylint -d c0301 tests controllers model nexflit_api utils
```

This will test the modules ``tests``,``controllers``,``model``,``nexflit_api`` and ``utils``.

### Flake8
To check the code with flake8, use this command : 

```shell script
python -m flake8
```

This will run static code analysis with flake8.

## Deployment with Docker

To deploy with docker, you simply have to docker-compose it, we've done all the work for you :
```shell script
docker-compose up --build
```

Omit the ``--build`` flag if you don't need to rebuild the Docker image (e.g. If no files have
changed since the last build)

## Built With

* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - The web framework used
* [Flask RESTful](https://flask-restful.readthedocs.io/en/latest/) - REST request made easy
* [Pylint](https://www.pylint.org/) - Code analysis for Python
* [Flake8](https://flake8.pycqa.org/en/latest/) - Tool for style guide enforcement
* [Docker](https://www.docker.com/) - Containerization software
* [Unittest](https://docs.python.org/3/library/unittest.html) - Unit testing framework
* [GitLab CI](https://docs.gitlab.com/ee/ci/) - CI platform to build and test applications

## Versioning

We try to use [SemVer](http://semver.org/) for versioning. 
For the versions available, see the [tags on this repository
](https://gitlab.com/M0ustach3/nexflit/-/tags). 

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details