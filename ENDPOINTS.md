# Endpoints
All of the requests __MUST__ be made raw with Content/JSON.
# Genres
### Data structure 
````
{
    "name" : "genreName"
}
````

### Endpoints
``GET http://localhost:5000/genres`` gives all genres in an array

``GET http://localhost:5000/<int:genre_id>`` give the genre with the id ``genre_id``

``POST http://localhost:5000/genres`` creates a new genre

``PUT http://localhost:5000/genres/<int:genre_id>`` modifies the genre with the ``genre_id``

``DELETE http://localhost:5000/<int:genre_id>`` deletes the genre with the ``genre_id``

# Users
### Data structure
````
{
            "email": "lol@lol.lol",
            "password": "superPasswordQuiVaEtreHashPlusTard",
            "isPremium": true,
            "isAdmin": true,
            "firstName": "Oué",
            "lastName": "Quelle indignité",
            "address": "oui"
}
````

### Endpoints

``GET http://localhost:5000/users`` gives all users

``GET http://localhost:5000/users/<int:user_id>`` gives the user with the ``user_id``

``POST http://localhost:5000/users`` creates a new user

``PUT http://localhost:5000/users/<int:user_id>`` modifies the user with the ``user_id``

``DELETE http://localhost:5000/users/<int:user_id>`` deletes the user with the ``user_id``

``POST http://localhost:5000/users/<int:user_id>/watchlist`` adds ids of content to the user's watchlist

``DELETE http://localhost:5000/users/<int:user_id>/watchlist`` removes ids of content from the user's watchlist

``POST http://localhost:5000/users/<int:user_id>/likes`` adds ids of content to the user's likes

``DELETE http://localhost:5000/users/<int:user_id>/watchlist`` removes ids of content from the user's likes

# Films
### Data structure
````
{
    "name": "Super film",
    "realisatorFirstName": "REALISATOR",
    "realisatorLastName": "DE OUF",
    "likes": 0,
    "synopsis": "Super synopsis",
    "type": "film",
    "genres": [],
    "length": 50,
    "releaseDate": 1999
}
````

### Endpoints

``GET http://localhost:5000/films`` gives all films

``GET http://localhost:5000/films/<int:film_id>`` gives the film with the ``film_id``

``POST http://localhost:5000/films`` creates a new film

``PUT http://localhost:5000/films/<int:film_id>`` modifies the film with the ``film_id``

``DELETE http://localhost:5000/films/<int:film_id>`` deletes the film with the ``film_id``

``POST http://localhost:5000/films/<int:film_id>/genres`` adds ids of genres to the film

``DELETE http://localhost:5000/films/<int:film_id>/genres`` removes ids of genres to the film


# Series
### Data structure
````
{
    "name": "Super série de ouf",
    "startDate" : 1999,
    "realisatorFirstName": "REALISATOR",
    "realisatorLastName": "DINGUE",
    "likes" : 0,
    "synopsis" : "Super synopsis",
    "numberOfSeasons": 0,
    "seasons" : []
}
````

### Endpoints

``GET http://localhost:5000/series`` gives all series

``GET http://localhost:5000/series/<int:serie_id>`` gives the serie with the ``serie_id``

``POST http://localhost:5000/series`` creates a new serie

``PUT http://localhost:5000/series/<int:serie_id>`` modifies the film with the ``serie_id``

``DELETE http://localhost:5000/series/<int:serie_id>`` deletes the film with the ``serie_id``

``POST http://localhost:5000/series/<int:serie_id>/genres`` adds ids of genres to the serie

``DELETE http://localhost:5000/series/<int:serie_id>/genres`` removes ids of genres to the serie

``POST http://localhost:5000/series/<int:serie_id>/seasons`` adds ids of seasons to the serie

``DELETE http://localhost:5000/series/<int:serie_id>/seasons`` removes ids of seasons to the serie


# Seasons
### Data structure
````
{
    "startDate" : 5,
    "numberOfEpisodes" : 0,
    "seasonNumber" : 1,
    "episodes" : []
}
````

### Endpoints

``GET http://localhost:5000/seasons`` gives all seasons

``GET http://localhost:5000/seasons/<int:season_id>`` gives the season with the ``season_id``

``POST http://localhost:5000/seasons`` creates a new season

``PUT http://localhost:5000/seasons/<int:season_id>`` modifies the season with the ``season_id``

``DELETE http://localhost:5000/seasons/<int:season_id>`` deletes the season with the ``season_id``

``POST http://localhost:5000/seasons/<int:season_id>/episodes`` adds ids of episodes to the serie

``DELETE http://localhost:5000/seasons/<int:season_id>/episodes`` removes ids of episodes to the serie

# Episodes
### Data structure
````
{
    "length" : 5,
    "releaseDate" : 5,
    "episodeName" : "wow dingue"
}
````

### Endpoints
``GET http://localhost:5000/episodes`` gives all episodes

``GET http://localhost:5000/episodes/<int:episode_id>`` gives the episode with the ``episode_id``

``POST http://localhost:5000/episodes`` creates a new episode

``PUT http://localhost:5000/episodes/<int:season_id>`` modifies the episode with the ``episode_id``

``DELETE http://localhost:5000/episodes/<int:season_id>`` deletes the episode with the ``episode_id``