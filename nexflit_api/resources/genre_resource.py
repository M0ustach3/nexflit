#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from flask import request
from flask_restful import Resource

from nexflit_api.resources import genre_controller
from utils.exceptions import Error, ResourceNotFound


class GenreResource(Resource):
    def get(self, genre_id):
        try:
            return genre_controller.get_genre(genre_id)
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found

    def put(self, genre_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            try:
                return genre_controller.update_genre(genre_id, _data)
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "An error occured"}, 500  # HTTP 404 code is server error

    def delete(self, genre_id):
        try:
            return genre_controller.delete_genre(genre_id)
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
        except Error:
            return {"error": "An error occured"}, 500  # HTTP 500 code is server error


class GenreListResource(Resource):
    def get(self):
        return genre_controller.list_genres()

    def post(self):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            try:
                return genre_controller.create_genre(_data), 201  # HTTP 201 code is created
            except Error:
                return {"error": "This genre already exists"}, 409  # HTTP 409 code is conflict
