#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from flask import request
from flask_restful import Resource

from nexflit_api.resources import user_controller
from utils.exceptions import ResourceNotFound, Error


class UserResource(Resource):
    def get(self, user_id):
        try:
            return user_controller.get_user(user_id)
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found

    def put(self, user_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            try:
                return user_controller.update_user(user_id, request.get_json())
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error

    def delete(self, user_id):
        try:
            return user_controller.delete_user(user_id)
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found


class UserListResource(Resource):
    def get(self):
        return user_controller.list_users()

    def post(self):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            mandatory_params = ['email', 'password', 'firstName', 'lastName']

            for param in mandatory_params:
                if param not in _data:
                    return {"error", "User must have {}".format(param)}, 400  # HTTP 400 code is bad request

            if ('isAdmin' not in _data) or (not isinstance(_data['isAdmin'], (bool,))):
                _data['isAdmin'] = False

            if ('isPremium' not in _data) or (not isinstance(_data['isPremium'], (bool,))):
                _data['isPremium'] = False

            if 'address' not in _data:
                _data['address'] = None
            try:
                return user_controller.create_user(_data), 201  # HTTP 201 code is created
            except Error:
                return {"error": "A user exists with this email"}, 409  # HTTP 409 code is conflict


class UserWatchlistResource(Resource):
    def post(self, user_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            try:
                if not request.get_json()['content_id']:
                    return {"error": "Please give a content_id in the body"}, 400  # HTTP 404 code is bad request
                content_id = request.get_json()['content_id']
                return user_controller.add_content_to_watchlist(content_id=content_id, user_id=user_id,
                                                                content_type=None)
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error

    def delete(self, user_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            try:
                if not request.get_json()['content_id']:
                    return {"error": "Please give a content_id in the body"}, 400  # HTTP 404 code is bad request
                content_id = request.get_json()['content_id']
                return user_controller.remove_content_from_watchlist(content_id=content_id, user_id=user_id,
                                                                     content_type=None)
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error


class UserLikesResource(Resource):
    def post(self, user_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            try:
                if not request.get_json()['content_id']:
                    return {"error": "Please give a content_id in the body"}, 400  # HTTP 404 code is bad request
                content_id = request.get_json()['content_id']
                return user_controller.add_content_to_likes(content_id=content_id, user_id=user_id,
                                                            content_type=None)
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error

    def delete(self, user_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            try:
                if not request.get_json()['content_id']:
                    return {"error": "Please give a content_id in the body"}, 400  # HTTP 404 code is bad request
                content_id = request.get_json()['content_id']
                return user_controller.remove_content_from_likes(content_id=content_id, user_id=user_id,
                                                                 content_type=None)
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error
