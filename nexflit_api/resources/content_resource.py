#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from flask import request
from flask_restful import Resource

from nexflit_api.resources import content_controller
from utils.exceptions import Error, ResourceNotFound


class FilmResource(Resource):
    def get(self, film_id):
        try:
            return content_controller.get_content(content_id=film_id, content_type='film')
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found

    def put(self, film_id):
        if request.is_json:
            _data = request.get_json()
            try:
                return content_controller.update_content(film_id, data=_data, content_type='film')
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error
        else:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request

    def delete(self, film_id):
        try:
            return content_controller.delete_content(content_id=film_id, content_type='film')
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
        except Error:
            return {"error": "A server error occured"}, 500  # HTTP 500 code is server error


class FilmListResource(Resource):
    def get(self):
        return content_controller.list_content(content_type='film')

    def post(self):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 400 code is bad request
        else:
            _data = request.get_json()
            try:
                return content_controller.create_content(data=_data, content_type='film'), 201
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error


class FilmGenreResource(Resource):
    def post(self, film_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            if not _data['genres']:
                return {"error": "Please put a genres attribute"}, 400  # HTTP 400 code is bad request
            try:
                content = None
                for genre_id in _data['genres']:
                    content = content_controller.add_genre_content(film_id, genre_id, content_type='film')
                return content, 200
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error

    def delete(self, film_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            if not _data['genres']:
                return {"error": "Please put a genres attribute"}, 400  # HTTP 400 code is bad request
            try:
                content = None
                for genre_id in _data['genres']:
                    content = content_controller.delete_genre_content(film_id, genre_id, content_type='film')
                return content, 200
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error


class SerieListResource(Resource):
    def get(self):
        return content_controller.list_content(content_type='serie')

    def post(self):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            try:
                return content_controller.create_content(data=_data, content_type='serie'), 201
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error


class SerieResource(Resource):
    def get(self, serie_id):
        try:
            return content_controller.get_content(content_id=serie_id, content_type='serie')
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found

    def put(self, serie_id):
        if request.is_json:
            _data = request.get_json()
            try:
                return content_controller.update_content(serie_id, data=_data, content_type='serie')
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error
        else:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request

    def delete(self, serie_id):
        try:
            return content_controller.delete_content(content_id=serie_id, content_type='serie')
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
        except Error:
            return {"error": "A server error occured"}, 500  # HTTP 500 code is server error


class SerieSeasonResource(Resource):
    def post(self, serie_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            if not _data['seasons']:
                return {"error": "Please put a seasons attribute"}, 400  # HTTP 400 code is bad request
            try:
                serie = None
                for season_id in _data['seasons']:
                    serie = content_controller.add_season_to_serie(season_id, serie_id)
                return serie, 200
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error

    def delete(self, serie_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            if not _data['seasons']:
                return {"error": "Please put a seasons attribute"}, 400  # HTTP 400 code is bad request
            try:
                serie = None
                for season_id in _data['seasons']:
                    serie = content_controller.remove_season_from_serie(season_id, serie_id)
                return serie, 200
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error


class SerieGenreResource(Resource):
    def post(self, serie_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            if not _data['genres']:
                return {"error": "Please put a genres attribute"}, 400  # HTTP 400 code is bad request
            try:
                content = None
                for genre_id in _data['genres']:
                    content = content_controller.add_genre_content(serie_id, genre_id, content_type='serie')
                return content, 200
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error

    def delete(self, serie_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            if not _data['genres']:
                return {"error": "Please put a genres attribute"}, 400  # HTTP 400 code is bad request
            try:
                content = None
                for genre_id in _data['genres']:
                    content = content_controller.delete_genre_content(serie_id, genre_id, content_type='serie')
                return content, 200
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error
