#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from flask import request
from flask_restful import Resource

from nexflit_api.resources import episode_controller
from utils.exceptions import Error, ResourceNotFound


class EpisodeResource(Resource):
    def get(self, episode_id):
        try:
            return episode_controller.get_episode(episode_id)
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found

    def put(self, episode_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            try:
                return episode_controller.update_episode(episode_id, _data)
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "An error occured"}, 500  # HTTP 404 code is server error

    def delete(self, episode_id):
        try:
            return episode_controller.delete_episode(episode_id)
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
        except Error:
            return {"error": "An error occured"}, 500  # HTTP 500 code is server error


class EpisodeListResource(Resource):
    def get(self):
        return episode_controller.list_episodes()

    def post(self):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            try:
                return episode_controller.create_episode(_data), 201  # HTTP 201 code is created
            except Error:
                return {"error": "This genre already exists"}, 409  # HTTP 409 code is conflict
