from controllers.content_controller import ContentController
from controllers.episode_controller import EpisodeController
from controllers.genre_controller import GenreController
from controllers.season_controller import SeasonController
from controllers.user_contoller import UserController
from model.database import DatabaseEngine
from settings import DATABASE_FILE_NAME, DATABASE_ENGINE

database_engine = DatabaseEngine(url='{}:///{}'.format(DATABASE_ENGINE, DATABASE_FILE_NAME), verbose=False)
database_engine.create_database()

# Controller init
genre_controller = GenreController(database_engine)
content_controller = ContentController(database_engine)
user_controller = UserController(database_engine)
episode_controller = EpisodeController(database_engine)
season_controller = SeasonController(database_engine)
