#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from flask import request
from flask_restful import Resource

from nexflit_api.resources import season_controller
from utils.exceptions import Error, ResourceNotFound


class SeasonResource(Resource):
    def get(self, season_id):
        try:
            return season_controller.get_season(season_id)
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found

    def put(self, season_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            try:
                return season_controller.update_season(season_id, _data)
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "An error occured"}, 500  # HTTP 404 code is server error

    def delete(self, season_id):
        try:
            return season_controller.delete_season(season_id)
        except ResourceNotFound:
            return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
        except Error:
            return {"error": "An error occured"}, 500  # HTTP 500 code is server error


class SeasonListResource(Resource):
    def get(self):
        return season_controller.list_seasons()

    def post(self):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            try:
                return season_controller.create_season(_data).to_dict(), 201  # HTTP 201 code is created
            except Error:
                return {"error": "This genre already exists"}, 409  # HTTP 409 code is conflict


class SeasonEpisodeResource(Resource):
    def post(self, season_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            if not _data['episodes']:
                return {"error": "Please put a episodes attribute"}, 400  # HTTP 400 code is bad request
            try:
                season = None
                for episode_id in _data['episodes']:
                    season = season_controller.add_episode_to_season(episode_id, season_id)
                return season, 200
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error

    def delete(self, season_id):
        if not request.is_json:
            return {"error": "Please put the data in JSON format"}, 400  # HTTP 404 code is bad request
        else:
            _data = request.get_json()
            if not _data['episodes']:
                return {"error": "Please put a episodes attribute"}, 400  # HTTP 400 code is bad request
            try:
                season = None
                for episode_id in _data['episodes']:
                    season = season_controller.remove_episode_from_season(episode_id, season_id)
                return season, 200
            except ResourceNotFound:
                return {"error": "Resource not found"}, 404  # HTTP 404 code is not found
            except Error:
                return {"error": "A server error occured"}, 500  # HTTP 500 code is server error
