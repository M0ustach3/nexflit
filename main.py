#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from flask import Flask
from flask_restful import Api

from nexflit_api.resources.content_resource import FilmListResource, SerieListResource, FilmResource, SerieResource, \
    FilmGenreResource, SerieSeasonResource, SerieGenreResource
from nexflit_api.resources.episode_resource import EpisodeResource, EpisodeListResource
from nexflit_api.resources.genre_resource import GenreListResource, GenreResource
from nexflit_api.resources.season_resource import SeasonListResource, SeasonResource, SeasonEpisodeResource
from nexflit_api.resources.user_resource import UserResource, UserListResource, UserWatchlistResource, UserLikesResource

app = Flask(__name__)
api = Api(app)

# Genres
api.add_resource(GenreListResource, '/genres')
api.add_resource(GenreResource, '/genres/<int:genre_id>')

# Users
api.add_resource(UserListResource, '/users')
api.add_resource(UserResource, '/users/<int:user_id>')
api.add_resource(UserWatchlistResource, '/users/<int:user_id>/watchlist')
api.add_resource(UserLikesResource, '/users/<int:user_id>/likes')

# Films
api.add_resource(FilmListResource, '/films')
api.add_resource(FilmResource, '/films/<int:film_id>')
api.add_resource(FilmGenreResource, '/films/<int:film_id>/genres')

# Series
api.add_resource(SerieListResource, '/series')
api.add_resource(SerieResource, '/series/<int:serie_id>')
api.add_resource(SerieSeasonResource, '/series/<int:serie_id>/seasons')
api.add_resource(SerieGenreResource, '/series/<int:serie_id>/genres')

# Seasons
api.add_resource(SeasonListResource, '/seasons')
api.add_resource(SeasonResource, '/seasons/<int:season_id>')
api.add_resource(SeasonEpisodeResource, '/seasons/<int:season_id>/episodes')

# Episodes
api.add_resource(EpisodeResource, '/episodes/<int:episode_id>')
api.add_resource(EpisodeListResource, '/episodes')

if __name__ == '__main__':
    app.run(debug=True)
