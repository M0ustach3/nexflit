#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from sqlalchemy.exc import IntegrityError, SQLAlchemyError
from sqlalchemy.orm.exc import NoResultFound

from utils.exceptions import ResourceNotFound, Error


def dao_error_handler(func):
    """
    Decorator pattern
    https://www.python.org/dev/peps/pep-0318/
    """

    def handler(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except NoResultFound:
            raise ResourceNotFound("Resource not found")
        except IntegrityError:
            raise Error("Error data may be malformed")
        except SQLAlchemyError as e:
            raise Error("An error occurred (%s)" % str(e))

    return handler
