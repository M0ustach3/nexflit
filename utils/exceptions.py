#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class ResourceNotFound(Error):
    pass


class InvalidData(Error):
    pass
