#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from model.dao.content_dao_fabric import ContentDAOFabric
from model.dao.genre_dao import GenreDAO
from utils.exceptions import Error, ResourceNotFound


class GenreController:
    def __init__(self, database_engine):
        self._database_engine = database_engine

    def list_genres(self):
        with self._database_engine.new_session() as session:
            genres = GenreDAO(session).get_all()
            genres_data = [genre.to_dict() for genre in genres]
        return genres_data

    def create_genre(self, data):
        try:
            with self._database_engine.new_session() as session:
                genre = GenreDAO(session).create(data)
                genre_data = genre.to_dict()
            return genre_data
        except Error as e:
            raise e

    def get_genre(self, genre_id):
        try:
            with self._database_engine.new_session() as session:
                return GenreDAO(session).get(genre_id).to_dict()
        except ResourceNotFound as e:
            raise e

    def update_genre(self, genre_id, data):
        try:
            with self._database_engine.new_session() as session:
                try:
                    genre_dao = GenreDAO(session)
                    genre = genre_dao.get(genre_id)
                    return genre_dao.update(genre, data).to_dict()
                except ResourceNotFound as e:
                    raise e
        except Error as e:
            raise e

    def delete_genre(self, genre_id):
        try:
            with self._database_engine.new_session() as session:
                genre_dao = GenreDAO(session)
                genre = genre_dao.get(genre_id)
                return genre_dao.delete(genre).to_dict()
        except ResourceNotFound as e1:
            raise e1
        except Error as e:
            raise e

    def add_content_to_genre(self, content_id, genre_id, content_type):
        with self._database_engine.new_session() as session:
            content = ContentDAOFabric(session).get_dao(content_type).get(content_id)
            genre = GenreDAO(session).get(genre_id)
            genre.add_content(content=content, session=session)
            return genre.to_dict()

    def remove_content_to_genre(self, content_id, genre_id, content_type):
        with self._database_engine.new_session() as session:
            content = ContentDAOFabric(session).get_dao(content_type).get(content_id)
            genre = GenreDAO(session).get(genre_id)
            genre.delete_content(content=content, session=session)
            return genre.to_dict()
