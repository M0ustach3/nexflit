#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from model.dao.content_dao_fabric import ContentDAOFabric
from model.dao.genre_dao import GenreDAO
from model.dao.season_dao import SeasonDAO
from utils.exceptions import Error


class ContentController:
    def __init__(self, database_engine):
        self._database_engine = database_engine

    def list_content(self, content_type=None):
        with self._database_engine.new_session() as session:
            contents = ContentDAOFabric(session).get_dao(_type=content_type).get_all()
            contents_data = [ct.to_dict() for ct in contents]
        return contents_data

    def create_content(self, data, content_type=None):
        try:
            with self._database_engine.new_session() as session:
                content = ContentDAOFabric(session).get_dao(_type=content_type).create(data)
                content = content.to_dict()
            return content
        except Error as e:
            raise e

    def get_content(self, content_id, content_type=None):
        with self._database_engine.new_session() as session:
            dao = ContentDAOFabric(session).get_dao(_type=content_type)
            content = dao.get(content_id)
            content = content.to_dict()
        return content

    def update_content(self, content_id, data, content_type):
        with self._database_engine.new_session() as session:
            dao = ContentDAOFabric(session).get_dao(content_type)
            content = dao.get(content_id)
            content = dao.update(content, data)
            return content.to_dict()

    def delete_genre_content(self, content_id, genre_id, content_type=None):
        with self._database_engine.new_session() as session:
            dao = ContentDAOFabric(session).get_dao(_type=content_type)
            content = dao.get(content_id)
            genre = GenreDAO(session).get(genre_id)
            content.delete_genre(genre, session)
            return content.to_dict()

    def add_genre_content(self, content_id, genre_id, content_type=None):
        with self._database_engine.new_session() as session:
            dao = ContentDAOFabric(session).get_dao(_type=content_type)
            content = dao.get(content_id)
            genre = GenreDAO(session).get(genre_id)
            content.add_genre(genre, session)
            return content.to_dict()

    def delete_content(self, content_id, content_type=None):
        with self._database_engine.new_session() as session:
            dao = ContentDAOFabric(session).get_dao(_type=content_type)
            content = dao.get(content_id)
            return dao.delete(content).to_dict()

    def search_content(self, content_name, content_type=None):
        with self._database_engine.new_session() as session:
            dao = ContentDAOFabric(session).get_dao(content_type)
            content = dao.get_by_name(content_name)
            return content.to_dict()

    def add_season_to_serie(self, season_id, serie_id):
        with self._database_engine.new_session() as session:
            dao = ContentDAOFabric(session).get_dao("serie")
            serie = dao.get(serie_id)
            season = SeasonDAO(session).get(season_id)
            serie.add_season(season, session)
            return serie.to_dict()

    def remove_season_from_serie(self, season_id, serie_id):
        with self._database_engine.new_session() as session:
            serie = ContentDAOFabric(session).get_dao("serie").get(serie_id)
            season = SeasonDAO(session).get(season_id)
            serie.delete_season(season=season, session=session)
            return serie.to_dict()
