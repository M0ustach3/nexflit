#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from model.dao.content_dao_fabric import ContentDAOFabric
from model.dao.user_dao import UserDAO
from utils.exceptions import Error, ResourceNotFound, InvalidData


class UserController:
    def __init__(self, database_engine):
        self._database_engine = database_engine

    def list_users(self):
        with self._database_engine.new_session() as session:
            users = UserDAO(session).get_all()
            users_data = [user.to_dict() for user in users]
        return users_data

    def create_user(self, data):
        mandatory_params = ['email', 'password', 'firstName', 'lastName']

        for param in mandatory_params:
            if param not in data:
                raise InvalidData("Missing param {}".format(param))

        try:
            with self._database_engine.new_session() as session:
                user = UserDAO(session).create(data)
                user_data = user.to_dict()
            return user_data
        except Error as e:
            raise e

    def get_user(self, user_id):
        with self._database_engine.new_session() as session:
            return UserDAO(session).get(user_id).to_dict()

    def delete_user(self, user_id):
        try:
            with self._database_engine.new_session() as session:
                userDao = UserDAO(session)
                user = userDao.get(user_id)
                return userDao.delete(user).to_dict()
        except ResourceNotFound as e1:
            raise e1
        except Error as e:
            raise e

    def add_content_to_watchlist(self, content_id, user_id, content_type):
        with self._database_engine.new_session() as session:
            content = ContentDAOFabric(session).get_dao(content_type).get(content_id)
            user = UserDAO(session).get(user_id)
            user.add_content_to_watchlist(content=content, session=session)
            return user.to_dict()

    def remove_content_from_watchlist(self, content_id, user_id, content_type):
        with self._database_engine.new_session() as session:
            content = ContentDAOFabric(session).get_dao(content_type).get(content_id)
            user = UserDAO(session).get(user_id)
            user.remove_content_from_watchlist(content=content, session=session)
            return user.to_dict()

    def update_user(self, user_id, data):
        try:
            with self._database_engine.new_session() as session:
                userDao = UserDAO(session)
                user = userDao.get(user_id)
                user = userDao.update(user, data)
                return user.to_dict()
        except Error as e:
            raise e

    def add_content_to_likes(self, content_id, user_id, content_type):
        with self._database_engine.new_session() as session:
            content = ContentDAOFabric(session).get_dao(content_type).get(content_id)
            user = UserDAO(session).get(user_id)
            user.add_content_to_likes(content=content, session=session)
            return user.to_dict()

    def remove_content_from_likes(self, content_id, user_id, content_type):
        with self._database_engine.new_session() as session:
            content = ContentDAOFabric(session).get_dao(content_type).get(content_id)
            user = UserDAO(session).get(user_id)
            user.remove_content_from_likes(content=content, session=session)
            return user.to_dict()
