#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from model.dao.content_dao_fabric import ContentDAOFabric
from model.dao.episode_dao import EpisodeDAO
from model.dao.genre_dao import GenreDAO
from model.dao.season_dao import SeasonDAO
from utils.exceptions import Error, ResourceNotFound


class SeasonController:
    def __init__(self, database_engine):
        self._database_engine = database_engine

    def list_seasons(self):
        with self._database_engine.new_session() as session:
            seasons = SeasonDAO(session).get_all()
            seasons_data = [season.to_dict() for season in seasons]
        return seasons_data

    def create_season(self, data):
        try:
            with self._database_engine.new_session() as session:
                season = SeasonDAO(session).create(data)
                season_data = season
            return season_data
        except Error as e:
            raise e

    def get_season(self, season_id):
        try:
            with self._database_engine.new_session() as session:
                return SeasonDAO(session).get(season_id).to_dict()
        except ResourceNotFound as e:
            raise e

    def update_season(self, season_id, data):
        try:
            with self._database_engine.new_session() as session:
                try:
                    season_dao = SeasonDAO(session)
                    season = season_dao.get(season_id)
                    return season_dao.update(season, data).to_dict()
                except ResourceNotFound as e:
                    raise e
        except Error as e:
            raise e

    def delete_season(self, season_id):
        try:
            with self._database_engine.new_session() as session:
                season_dao = SeasonDAO(session)
                season = season_dao.get(season_id)
                return season_dao.delete(season).to_dict()
        except ResourceNotFound as e1:
            raise e1
        except Error as e:
            raise e

    def add_episode_to_season(self, episode_id, season_id):
        with self._database_engine.new_session() as session:
            episode = EpisodeDAO(session).get(episode_id)
            season = SeasonDAO(session).get(season_id)
            season.add_episode(episode=episode, session=session)
            return episode.to_dict()

    def remove_episode_from_season(self, episode_id, season_id):
        with self._database_engine.new_session() as session:
            episode = EpisodeDAO(session).get(episode_id)
            season = SeasonDAO(session).get(season_id)
            season.delete_episode(episode=episode, session=session)
            return episode.to_dict()
