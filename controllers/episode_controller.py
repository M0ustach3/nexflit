#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from model.dao.content_dao_fabric import ContentDAOFabric
from model.dao.episode_dao import EpisodeDAO
from model.dao.genre_dao import GenreDAO
from model.dao.season_dao import SeasonDAO
from utils.exceptions import Error, ResourceNotFound


class EpisodeController:
    def __init__(self, database_engine):
        self._database_engine = database_engine

    def list_episodes(self):
        with self._database_engine.new_session() as session:
            episodes = EpisodeDAO(session).get_all()
            episodes_data = [genre.to_dict() for genre in episodes]
        return episodes_data

    def create_episode(self, data):
        try:
            with self._database_engine.new_session() as session:
                episode = EpisodeDAO(session).create(data)
                episode_data = episode.to_dict()
            return episode_data
        except Error as e:
            raise e

    def get_episode(self, episode_id):
        try:
            with self._database_engine.new_session() as session:
                return EpisodeDAO(session).get(episode_id).to_dict()
        except ResourceNotFound as e:
            raise e

    def update_episode(self, episode_id, data):
        try:
            with self._database_engine.new_session() as session:
                try:
                    episode_dao = EpisodeDAO(session)
                    episode = episode_dao.get(episode_id)
                    return episode_dao.update(episode, data).to_dict()
                except ResourceNotFound as e:
                    raise e
        except Error as e:
            raise e

    def delete_episode(self, episode_id):
        try:
            with self._database_engine.new_session() as session:
                episode_dao = EpisodeDAO(session)
                episode = episode_dao.get(episode_id)
                return episode_dao.delete(episode).to_dict()
        except ResourceNotFound as e1:
            raise e1
        except Error as e:
            raise e