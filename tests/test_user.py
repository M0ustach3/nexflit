#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

import unittest

from controllers.user_contoller import UserController
from model.database import DatabaseEngine
from utils.exceptions import Error, InvalidData, ResourceNotFound


class TestUser(unittest.TestCase):

    def setUp(self) -> None:
        self.database_engine = DatabaseEngine(url='sqlite://', verbose=False)
        self.database_engine.create_database()
        self.user_controller = UserController(self.database_engine)

    def test_list_users(self):
        list_users = self.user_controller.list_users()
        self.assertIsNotNone(list_users)
        self.assertIs(0, len(list_users))

    def test_create_user(self):
        list_users = self.user_controller.list_users()
        self.assertIsNotNone(list_users)
        self.assertIs(0, len(list_users))
        user = self.user_controller.create_user({
            "email": "lol@lol.lol",
            "password": "superPasswordQuiVaEtreHashPlusTard",
            "isPremium": True,
            "isAdmin": True,
            "firstName": "Oué",
            "lastName": "Quelle indignité",
            "address": "oui"
        })
        self.assertIsNotNone(user)
        list_users = self.user_controller.list_users()
        self.assertIs(1, len(list_users))
        u1 = list_users[0]
        self.assertEqual(user, u1)
        user = self.user_controller.delete_user(1)
        self.assertEqual(user, u1)
        list_users = self.user_controller.list_users()
        self.assertIs(0, len(list_users))

    def test_create_wrong_user(self):
        with self.assertRaises(InvalidData):
            self.user_controller.create_user({
                "oulah": "pas beaucoup d'attributs ici"
            })

    def test_create_already_existing_user(self):
        self.user_controller.create_user({
            "email": "lol@lol.lol",
            "password": "superPasswordQuiVaEtreHashPlusTard",
            "isPremium": True,
            "isAdmin": True,
            "firstName": "Oué",
            "lastName": "Quelle indignité",
            "address": "oui"
        })
        with self.assertRaises(Error):
            self.user_controller.create_user({
                "email": "lol@lol.lol",
                "password": "superPasswordQuiVaEtreHashPlusTard",
                "isPremium": True,
                "isAdmin": True,
                "firstName": "Oué",
                "lastName": "Quelle indignité",
                "address": "oui"
            })
        self.user_controller.delete_user(1)

    def test_delete_user(self):
        self.user_controller.create_user({
            "email": "lol@lol.lol",
            "password": "superPasswordQuiVaEtreHashPlusTard",
            "isPremium": True,
            "isAdmin": True,
            "firstName": "Oué",
            "lastName": "Quelle indignité",
            "address": "oui"
        })
        list_users = self.user_controller.list_users()
        self.assertIs(1, len(list_users))
        self.user_controller.delete_user(1)
        list_users = self.user_controller.list_users()
        self.assertIs(0, len(list_users))

    def test_delete_non_existing_user(self):
        with self.assertRaises(ResourceNotFound):
            self.user_controller.delete_user(1)


if __name__ == '__main__':
    unittest.main()
