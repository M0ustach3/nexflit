FROM python:3

ADD . /app

WORKDIR /app

RUN apt-get update

RUN pip3 install -r requirements.txt

CMD python3 main.py