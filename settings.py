#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from dotenv import load_dotenv
import os

load_dotenv(verbose=False)

DATABASE_ENGINE = os.getenv("DATABASE_ENGINE")
DATABASE_FILE_NAME = os.getenv("DATABASE_FILE_NAME")
