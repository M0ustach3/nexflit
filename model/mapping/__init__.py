#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from sqlalchemy.ext.declarative import declarative_base

""" base class from which all mapped classes should inherit """
Base = declarative_base()
