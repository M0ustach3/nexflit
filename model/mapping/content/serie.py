#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from model.database import Session
from model.mapping.content.content import Content
from model.mapping.content.season import Season


class Serie(Content):
    """
    Class used to represent a serie in the database. This class inherits from the Content class.

    Name of the table : serie

    Attributes
    ----------------
    id: int
        The id of the serie
    startDate : int
        The start date of the serie (will probably be a timestamp)
    numberOfSeasons : int
        The number of seasons of the serie (its basically a calculated
        value for the length of the array of seasons)
    seasons : list
        The list of seasons of this serie
    """
    __tablename__ = 'serie'

    # Columns
    id: int = Column(Integer, ForeignKey('content.id'), primary_key=True, autoincrement=True)
    startDate: int = Column(Integer)
    numberOfSeasons: int = Column(Integer)

    # Relations
    seasons = relationship("Season", back_populates="serie")

    # Inheritance
    # See : https://docs.sqlalchemy.org/en/13/orm/inheritance.html
    __mapper_args__ = {
        'polymorphic_identity': 'serie'
    }

    def add_season(self, season: Season, session: Session) -> None:
        """
        Method used to add an season to a serie
        :param season: The season to add
        :param session: The database session to use
        :return: None
        """
        self.seasons.append(season)
        session.flush()

    def delete_season(self, season: Season, session: Session) -> None:
        """
        Method used to delete a season from a serie
        :param season: The season to remove
        :param session: The database session to use
        :return: None
        """
        season_assoc = None
        for assoc in self.seasons:
            if assoc == season:
                season_assoc = assoc
                break
        if season_assoc is not None:
            self.seasons.remove(season_assoc)
            session.delete(season_assoc)
            session.flush()

    def __repr__(self) -> str:
        return super().__repr__() + "Serie {} de {} saisons (débutée en {})".format(self.name,
                                                                                    self.numberOfSeasons,
                                                                                    self.startDate)

    def to_dict(self) -> object:
        """
        Method used to convert the serie to an object
        :rtype: object
        :return: the serie in an object form
        """
        _dict = super().to_dict()
        _dict['startDate'] = self.startDate
        _dict['numberOfSeasons'] = self.numberOfSeasons
        _dict['seasons'] = []
        for season in self.seasons:
            _dict['seasons'].append(season.to_dict())
        return _dict
