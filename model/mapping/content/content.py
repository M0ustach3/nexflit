#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from model.mapping import Base
from model.mapping.assoc.content_has_genres import ContentHasGenres


class Content(Base):
    """
    Class used to represent a content in the database. This class is the parent class of the Film and Serie class.

    Table name : content


    Attributes
    ------------
    id : int
        The id of the content
    name : str
        The name of the content
    realisatorFirstName : str
        The first name of the realisator
    realisatorLastName : str
        The last name of the realisator
    content_type : int
        The type of the content. THIS SERVES AS THE DISCRIMINANT FOR THE INHERITANCE OF SQLALCHEMY
    likes : int
        The number of likes of this content
    synopsis : str
        The synopsis of the content

    """
    __tablename__ = 'content'

    # Columns
    id: int = Column(Integer, autoincrement=True, primary_key=True)
    name: str = Column(String(100))
    realisatorFirstName: str = Column(String(100))
    realisatorLastName: str = Column(String(100))
    content_type: int = Column(Integer, nullable=False)
    likes: int = Column(Integer)
    synopsis: str = Column(String)

    # Inheritance
    __mapper_args__ = {
        'polymorphic_identity': 'content',
        'polymorphic_on': content_type
    }

    # Relations
    genres = relationship("ContentHasGenres", back_populates="content_assoc")
    liked_by = relationship("UserLikesContent", back_populates="content_assoc")
    watched_by = relationship("UserWatchesContent", back_populates="content_assoc")

    def __repr__(self) -> str:
        return "Content : {} with id {} and {} likes".format(self.name, self.id, self.likes)

    def to_dict(self):
        """
        Method used to convert a content to an object
        :return: An object representing the content
        """
        _data = {
            "id": self.id,
            "name": self.name,
            "realisatorFirstName": self.realisatorFirstName,
            "realisatorLastName": self.realisatorLastName,
            "likes": self.likes,
            "synopsis": self.synopsis,
            "type": self.content_type,
            "genres": []
        }
        for genre_association in self.genres:
            _data["genres"].append(genre_association.genre_assoc.to_dict())
        return _data

    def delete_genre(self, genre, session):
        genre_association = None
        for association in self.genres:
            if association.genre_assoc == genre:
                genre_association = association
                break
        if genre_association is not None:
            self.genres.remove(genre_association)
            session.delete(genre_association)
            session.flush()

    def add_genre(self, genre, session):
        genre_association = None
        for association in self.genres:
            if association.genre_assoc == genre:
                genre_association = association
                break
        if genre_association is None:
            genre_association = ContentHasGenres()
            genre_association.genre_assoc = genre
            self.genres.append(genre_association)
            session.flush()
