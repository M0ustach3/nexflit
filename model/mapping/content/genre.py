#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from model.database import Session
from model.mapping import Base
from model.mapping.assoc.content_has_genres import ContentHasGenres
from model.mapping.content.content import Content


class Genre(Base):
    """
    Class used to represent a genre in the database.

    Table name : genre


    Attributes
    -----------
    id : int
        The id of the genre
    name : str
        The name of the genre
    contents : List[Content]
        The contents that are of the specific genre

    """
    __tablename__ = 'genre'

    # Columns
    id: int = Column(Integer, autoincrement=True, primary_key=True)
    name: str = Column(String(50), unique=True)

    # Relations
    contents = relationship("ContentHasGenres", back_populates="genre_assoc")

    def __repr__(self) -> str:
        return "Genre {}".format(self.name)

    def to_dict(self) -> object:
        """
        Method used to convert a genre to an object
        :rtype: object
        :return: The object representation of a genre
        """
        _data = {
            "id": self.id,
            "name": self.name,
            "contents": []
        }
        for genre_association in self.contents:
            _data["contents"].append({
                "id": genre_association.content_assoc.id,
                "name": genre_association.content_assoc.name,
                "realisatorFirstName": genre_association.content_assoc.realisatorFirstName,
                "realisatorLastName": genre_association.content_assoc.realisatorLastName,
                "likes": genre_association.content_assoc.likes,
                "synopsis": genre_association.content_assoc.synopsis,
                "type": genre_association.content_assoc.content_type
            })
        return _data

    def add_content(self, content: Content, session: Session) -> None:
        """
        Method used to add a content to a genre
        :param content: The new content to add
        :param session: The database session to use
        :return: None
        """
        assoc = ContentHasGenres()
        assoc.content_assoc = content
        self.contents.append(assoc)
        session.flush()

    def delete_content(self, content: Content, session: Session) -> None:
        """
        Method used to delete a content from the genre
        :param content: The content to remove
        :param session: The database session to use
        :return: None
        """
        content_assoc = None
        for assoc in self.contents:
            if assoc.content_assoc == content:
                content_assoc = assoc
                break
        if content_assoc is not None:
            self.contents.remove(content_assoc)
            session.delete(content_assoc)
            session.flush()
