#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from model.database import Session
from model.mapping import Base
from model.mapping.content.episode import Episode


class Season(Base):
    """
    Class used to represent a season in the database

    Table name : season

    Attributes
    ------------
    id : int
        The id of the season
    startDate : int
        The start date of the season (will probably be a timestamp)
    numberOfEpisodes : int
        The number of episodes of the season
    serie_id : int
        The id of the corresponding serie. This is a foreign key
    serie : Serie
        The corresponding serie of the season
    episodes : List[Episodes]
        The list of episodes that the serie contains
    """

    __tablename__ = 'season'

    # Columns
    id: int = Column(Integer, primary_key=True, autoincrement=True)
    startDate: int = Column(Integer)
    numberOfEpisodes: int = Column(Integer)
    serie_id: int = Column(Integer, ForeignKey("serie.id"))
    seasonNumber: int = Column(Integer)

    # Relations
    serie = relationship("Serie", back_populates="seasons")
    episodes = relationship("Episode", back_populates="season")

    def add_episode(self, episode: Episode, session: Session) -> None:
        """
        Method used to add an episode to a season
        :param episode: The episode to add
        :param session: The database session to use
        :return: None
        """
        self.episodes.append(episode)
        session.flush()

    def delete_episode(self, episode: Episode, session: Session) -> None:
        """
        Method used to delete an episode from a season
        :param episode: The episode to remove
        :param session: The database session to use
        :return: None
        """
        episode_assoc = None
        for assoc in self.episodes:
            if assoc == episode:
                episode_assoc = assoc
                break
        if episode_assoc is not None:
            self.episodes.remove(episode_assoc)
            session.delete(episode_assoc)
            session.flush()

    def __repr__(self) -> str:
        return "Saison de {} épisodes de la série {}".format(self.numberOfEpisodes, self.serie.__repr__())

    def to_dict(self) -> object:
        """
        Method used to convert a season to an object
        :rtype: object
        :return: The representation of the season
        """
        _data = {
            "id": self.id,
            "startDate": self.startDate,
            "numberOfEpisodes": self.numberOfEpisodes,
            "seasonNumber": self.seasonNumber,
            "episodes": []
        }

        for episode in self.episodes:
            _data["episodes"].append({
                "id": episode.id,
                "length": episode.length,
                "releaseDate": episode.releaseDate,
                "episodeName": episode.episodeName
            })
        return _data
