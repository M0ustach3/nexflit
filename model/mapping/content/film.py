#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from sqlalchemy import Column, Integer, ForeignKey

from model.mapping.content.content import Content


class Film(Content):
    """
    Class used to represent a film in the database. The class extends the base Content class

    Table name : film

    Attributes
    --------------
    id: int
        The id of the film
    length: int
        The length in minutes of the film
    releaseDate : int
        The date of release of the film, as a year

    """
    __tablename__ = 'film'

    # Columns
    id: int = Column(Integer, ForeignKey('content.id'), autoincrement=True, primary_key=True)
    length: int = Column(Integer)
    releaseDate: int = Column(Integer)

    # Inheritance
    __mapper_args__ = {
        'polymorphic_identity': 'film'
    }

    def __repr__(self) -> str:
        return super(Film, self).__repr__() + ' (THIS IS A FILM)'

    def to_dict(self):
        """
        Method used to convert the film to an object
        This calls the Content.to_dict() method to obtain all of the arguments of the content
        :return: an object to represent the film
        :rtype: object
        """
        _dict = super().to_dict()
        _dict['length'] = self.length
        _dict['releaseDate'] = self.releaseDate
        return _dict
