#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from model.mapping import Base


class Episode(Base):
    """
    Class used to represent an episode in the database

    Table name : episode

    Attributes
    ------------

    id : int
        The id of the episode
    length : int
        The length of the episode
    releaseDate : int
        The release date of the episode
    season_id : int
        The id of the corresponding season
    season : Season
        The season of the episode
    """
    __tablename__ = 'episode'

    # Columns
    id: int = Column(Integer, primary_key=True, autoincrement=True)
    length: int = Column(Integer)
    releaseDate: int = Column(Integer)
    episodeName: str = Column(String(150))
    season_id: int = Column(Integer, ForeignKey("season.id"))

    # Relations
    season = relationship("Season", back_populates="episodes")

    def __repr__(self) -> str:
        return "Episode de {} minutes de la saison {} : {}".format(self.length, self.season.__repr__(),
                                                                   self.episodeName)

    def to_dict(self) -> object:
        """
        Method used to represent an episode in the object format
        :return: An object representing the episode
        """
        _data = {
            "id": self.id,
            "length": self.length,
            "releaseDate": self.releaseDate,
            "episodeName": self.episodeName
        }

        return _data
