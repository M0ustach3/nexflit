#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

import model.mapping.content.content
import model.mapping.content.episode
import model.mapping.content.film
import model.mapping.content.genre
import model.mapping.content.season
import model.mapping.content.serie
