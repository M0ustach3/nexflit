#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from model.database import Session
from model.mapping import Base
from model.mapping.assoc.user_watches_content import UserWatchesContent
from model.mapping.assoc.user_likes_content import UserLikesContent
from model.mapping.content.content import Content


class User(Base):
    """
    Class used to represent a user in the database and in the model

    Name of the table : user

    Attributes
    -------------

    id : int
        The id of the user
    email : str
        The email of the user
    password : str
        The password of the user
    isPremium : bool
        If the user is a premium user or not
    firstName : str
        The first name of the user
    lastName : str
        The last name of the user
    address : str
        The address of the user
    likes : list
        The list of all the content that the user liked
    watches : list
        The list of all the content in the user's watchlist
    """

    __tablename__ = 'user'

    # Columns
    id: int = Column(Integer, primary_key=True, autoincrement=True)
    email: str = Column(String(50), nullable=False, unique=True)
    password: str = Column(String(50), nullable=False)
    isPremium: bool = Column(Boolean, nullable=False, default=False)
    isAdmin: bool = Column(Boolean, nullable=False, default=False)
    firstName: str = Column(String(50), nullable=False)
    lastName: str = Column(String(50), nullable=False)
    address: str = Column(String(150), nullable=True)

    # Relations
    likes = relationship("UserLikesContent", back_populates="user_assoc")
    watches = relationship("UserWatchesContent", back_populates="user_assoc")

    def __repr__(self) -> str:
        return "User {} {} (premium : {}, admin : {}) with email {}".format(self.firstName, self.lastName,
                                                                            self.isPremium, self.isAdmin,
                                                                            self.email)

    def to_dict(self) -> object:
        """
        Method used to convert the user to an object
        :rtype: object
        :return the data representing the user
        """
        _data = {
            "id": self.id,
            "email": self.email,
            "isPremium": self.isPremium,
            "isAdmin": self.isAdmin,
            "firstName": self.firstName,
            "lastName": self.lastName,
            "address": self.address,
            "likes": [],
            "watchlist": []
        }

        for like in self.likes:
            _data["likes"].append(
                like.content_assoc.to_dict()
            )
        for watch in self.watches:
            _data["watchlist"].append(
                watch.content_assoc.to_dict()
            )
        return _data

    def add_content_to_watchlist(self, content: Content, session: Session) -> None:
        """
        Method used to add add content to the user's watchlist
        :param session: The database session to use
        :param content: The content to add, a film or a series
        :return: None
        """
        content_assoc = None
        for association in self.watches:
            if association.content_assoc == content:
                content_assoc = association
                break
        if content_assoc is None:
            assoc = UserWatchesContent()
            assoc.content_assoc = content
            self.watches.append(assoc)
            session.flush()

    def remove_content_from_watchlist(self, content: Content, session: Session) -> None:
        """
        Method used to remove content from user's watchlist
        :param content: The content to remove
        :param session: The database session to use
        :return: None
        """
        content_assoc = None
        for assoc in self.watches:
            if assoc.content_assoc == content:
                content_assoc = assoc
                break
        if content_assoc is not None:
            self.watches.remove(content_assoc)
            session.delete(content_assoc)
            session.flush()

    def add_content_to_likes(self, content: Content, session: Session) -> None:
        """
        Method used to add add content to the user's likes
        :param session: The database session to use
        :param content: The content to add, a film or a series
        :return: None
        """
        content_assoc = None
        for association in self.likes:
            if association.content_assoc == content:
                content_assoc = association
                break
        if content_assoc is None:
            assoc = UserLikesContent()
            content.likes += 1
            assoc.content_assoc = content
            self.likes.append(assoc)
            session.merge(content)
            session.flush()

    def remove_content_from_likes(self, content: Content, session: Session) -> None:
        """
        Method used to remove content from user's likes
        :param content: The content to remove
        :param session: The database session to use
        :return: None
        """
        content_assoc = None
        for assoc in self.likes:
            if assoc.content_assoc == content:
                content_assoc = assoc
                break
        if content_assoc is not None:
            content.likes -= 1
            self.likes.remove(content_assoc)
            session.merge(content)
            session.delete(content_assoc)
            session.flush()
