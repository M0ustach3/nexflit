#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

import model.mapping.assoc.user_likes_content
import model.mapping.assoc.content_has_genres
import model.mapping.assoc.user_watches_content
