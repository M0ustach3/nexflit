#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from sqlalchemy import UniqueConstraint, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from model.mapping import Base


class UserWatchesContent(Base):
    """
    Association table used to connect the user and his watchlist

    Table name : user_watches_content

    """
    __tablename__ = 'user_watches_content'
    __table_args__ = (UniqueConstraint('content_id', 'user_id'),)

    content_id = Column(Integer, ForeignKey('content.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    content_assoc = relationship("Content", back_populates="watched_by")
    user_assoc = relationship("User", back_populates="watches")
