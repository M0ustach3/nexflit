#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from sqlalchemy import UniqueConstraint, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from model.mapping import Base


class ContentHasGenres(Base):
    """
    Association table used to link a content and his genres

    Table name : content_has_genres

    """
    __tablename__ = 'content_has_genres'
    __table_args__ = (UniqueConstraint('content_id', 'genre_id'),)

    content_id = Column(Integer, ForeignKey('content.id'), primary_key=True)
    genre_id = Column(Integer, ForeignKey('genre.id'), primary_key=True)
    content_assoc = relationship("Content", back_populates="genres")
    genre_assoc = relationship("Genre", back_populates="contents")
