#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from model.dao.dao import DAO
from model.mapping.content.episode import Episode
from utils.dao_error_handler import dao_error_handler


class EpisodeDAO(DAO):

    def __init__(self, database_session):
        super().__init__(database_session)
        self._type = Episode

    @dao_error_handler
    def get(self, the_id):
        return self._database_session.query(self._type).filter_by(id=the_id).one()

    @dao_error_handler
    def get_all(self):
        return self._database_session.query(self._type).order_by(self._type.id).all()

    @dao_error_handler
    def create(self, data: dict):
        episode = Episode(length=data.get("length"), releaseDate=data.get("releaseDate"), episodeName=data.get("episodeName"),
                    season_id=data.get("season_id"))
        self._database_session.add(episode)
        self._database_session.flush()
        return episode

    @dao_error_handler
    def update(self, episode: Episode, data: dict):
        super().update(episode, data)
        if 'length' in data:
            episode.length = data['length']
        if 'releaseDate' in data:
            episode.releaseDate = data['releaseDate']
        if 'episodeName' in data:
            episode.episodeName = data['episodeName']
        if 'season_id' in data:
            episode.season_id = data['season_id']

        self._database_session.merge(episode)
        self._database_session.flush()
        return episode

    @dao_error_handler
    def delete(self, entity):
        return super().delete(entity)
