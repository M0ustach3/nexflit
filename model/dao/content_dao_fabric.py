#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from model.dao.content_dao import ContentDAO
from model.dao.film_dao import FilmDAO
from model.dao.serie_dao import SerieDAO


class ContentDAOFabric:
    def __init__(self, database_session):
        self._database_session = database_session

    def get_dao(self, _type=None):
        if _type == 'film':
            return FilmDAO(self._database_session)
        elif _type == 'serie':
            return SerieDAO(self._database_session)
        else:
            return ContentDAO(self._database_session)
