#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from model.dao.dao import DAO
from model.mapping.content.season import Season
from utils.dao_error_handler import dao_error_handler


class SeasonDAO(DAO):

    def __init__(self, database_session):
        super().__init__(database_session)
        self._type = Season

    @dao_error_handler
    def get(self, the_id):
        return self._database_session.query(self._type).filter_by(id=the_id).one()

    @dao_error_handler
    def get_all(self):
        return self._database_session.query(self._type).order_by(self._type.id).all()

    @dao_error_handler
    def create(self, data: dict):
        season = Season(startDate=data.get("startDate"), numberOfEpisodes=data.get("numberOfEpisodes"), seasonNumber=data.get("seasonNumber"),
                    episodes=data.get("episodes", []))
        self._database_session.add(season)
        self._database_session.flush()
        return season

    @dao_error_handler
    def update(self, season: Season, data: dict):
        super().update(season, data)
        if 'startDate' in data:
            season.startDate = data['startDate']
        if 'numberOfEpisodes' in data:
            season.numberOfEpisodes = data['numberOfEpisodes']
        if 'seasonNumber' in data:
            season.seasonNumber = data['seasonNumber']

        self._database_session.merge(season)
        self._database_session.flush()
        return season

    @dao_error_handler
    def delete(self, entity):
        return super().delete(entity)
