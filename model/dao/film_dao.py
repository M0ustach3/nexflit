#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from model.dao.content_dao import ContentDAO
from model.mapping.content.film import Film
from utils.dao_error_handler import dao_error_handler


class FilmDAO(ContentDAO):

    def __init__(self, database_session):
        super().__init__(database_session)
        self._type = Film

    @dao_error_handler
    def get(self, the_id):
        return self._database_session.query(self._type).filter_by(id=the_id).one()

    @dao_error_handler
    def get_all(self):
        return self._database_session.query(self._type).order_by(self._type.name).all()

    @dao_error_handler
    def create(self, data: dict):
        film = Film(name=data.get("name"), length=data.get("length"), releaseDate=data.get("releaseDate"),
                    realisatorFirstName=data.get("realisatorFirstName"),
                    realisatorLastName=data.get("realisatorLastName"), likes=data.get("likes"),
                    synopsis=data.get("synopsis"), genres=data.get("genres", []))
        self._database_session.add(film)
        self._database_session.flush()
        return film

    @dao_error_handler
    def update(self, film: Film, data: dict):
        super().update(film, data)
        if 'length' in data:
            film.length = data['length']
        if 'releaseDate' in data:
            film.releaseDate = data['releaseDate']
        if 'realisatorFirstName' in data:
            film.realisatorFirstName = data['realisatorFirstName']
        if 'synopsis' in data:
            film.synopsis = data['synopsis']

        self._database_session.merge(film)
        self._database_session.flush()
        return film

    @dao_error_handler
    def delete(self, entity):
        return super().delete(entity)
