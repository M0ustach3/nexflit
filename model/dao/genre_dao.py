#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée
from sqlalchemy.exc import SQLAlchemyError

from model.dao.dao import DAO
from model.mapping.content.genre import Genre
from utils.dao_error_handler import dao_error_handler


class GenreDAO(DAO):

    def __init__(self, database_session):
        super().__init__(database_session)

    @dao_error_handler
    def get(self, the_id):
        return self._database_session.query(Genre).filter_by(id=the_id).one()

    @dao_error_handler
    def get_all(self):
        return self._database_session.query(Genre).order_by(Genre.name).all()
        pass

    @dao_error_handler
    def create(self, data: dict):
        genre = Genre(name=data.get("name"))
        self._database_session.add(genre)
        self._database_session.flush()
        return genre

    @dao_error_handler
    def update(self, genre: Genre, data: dict):
        if "name" in data:
            genre.name = data["name"]
            self._database_session.merge(genre)
            self._database_session.flush()
        return genre

    @dao_error_handler
    def delete(self, genre: Genre):
        try:
            self._database_session.delete(genre)
            self._database_session.commit()
            return genre
        except SQLAlchemyError as e:
            raise e
