#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

import hashlib

from sqlalchemy.exc import SQLAlchemyError

from model.dao.dao import DAO
from model.mapping.user import User
from utils.dao_error_handler import dao_error_handler


class UserDAO(DAO):

    def __init__(self, database_session):
        super().__init__(database_session)
        self._type = User

    @dao_error_handler
    def get(self, the_id):
        try:
            return self._database_session.query(self._type).filter_by(id=the_id).one()
        except SQLAlchemyError as e:
            raise e

    @dao_error_handler
    def get_all(self):
        try:
            return self._database_session.query(self._type).order_by(self._type.lastName).all()
        except SQLAlchemyError as e:
            raise e

    @dao_error_handler
    def create(self, data: dict):
        try:
            user = User(email=data.get("email"),
                        password=hashlib.sha512(data.get("password").encode('utf-8')).hexdigest(),
                        isPremium=data.get("isPremium"),
                        firstName=data.get("firstName"),
                        lastName=data.get("lastName"), address=data.get("address"))
            self._database_session.add(user)
            self._database_session.flush()
            return user
        except SQLAlchemyError as e:
            raise e

    @dao_error_handler
    def update(self, user: User, data: dict):
        try:
            if 'email' in data:
                user.email = data['email']
            if 'password' in data:
                user.password = hashlib.sha512(data['password'].encode('utf-8')).hexdigest()
            if 'isPremium' in data:
                user.isPremium = data['isPremium']
            if 'firstName' in data:
                user.firstName = data['firstName']
            if 'lastName' in data:
                user.lastName = data['lastName']
            if 'address' in data:
                user.address = data['address']

            self._database_session.merge(user)
            self._database_session.flush()
            return user
        except SQLAlchemyError as e:
            raise e

    @dao_error_handler
    def delete(self, entity):
        try:
            self._database_session.delete(entity)
            self._database_session.commit()
            return entity
        except SQLAlchemyError as e:
            raise e
