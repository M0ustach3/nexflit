#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from sqlalchemy.exc import SQLAlchemyError

from model.dao.dao import DAO
from model.mapping.content.content import Content
from utils.dao_error_handler import dao_error_handler


class ContentDAO(DAO):

    def __init__(self, database_session):
        super().__init__(database_session)

    @dao_error_handler
    def get(self, the_id):
        return self._database_session.query(Content).filter_by(id=the_id).one()

    @dao_error_handler
    def get_all(self):
        return self._database_session.query(Content).all()

    @dao_error_handler
    def get_by_name(self, content_name: str):
        return self._database_session.query(Content).filter_by(name=content_name).all()

    def create(self, data: dict):
        raise NotImplementedError()

    def update(self, content: Content, data: dict):
        if 'name' in data:
            content.name = data['name']
        if 'realisatorLastName' in data:
            content.realisatorLastName = data['realisatorLastName']
        if 'genres' in data:
            content.genres = data['genres']
        if 'likes' in data:
            content.likes = data['likes']
        self._database_session.merge(content)
        self._database_session.flush()
        return content

    @dao_error_handler
    def delete(self, entity):
        try:
            self._database_session.delete(entity)
            self._database_session.commit()
            return entity
        except SQLAlchemyError as e:
            raise e
