#  Copyright (c) 2020. GLPOO
#  Made with <3 by :
#  Pablo Bondia-Luttiau
#  Lennaïg Charron
#  Michaël Ghesquière
#  Théophile Hamelin
#  Samuel Lablée

from model.dao.content_dao import ContentDAO
from model.mapping.content.serie import Serie
from utils.dao_error_handler import dao_error_handler


class SerieDAO(ContentDAO):
    def __init__(self, database_session):
        super().__init__(database_session)
        self._type = Serie

    @dao_error_handler
    def get(self, the_id):
        return self._database_session.query(self._type).filter_by(id=the_id).one()

    @dao_error_handler
    def get_all(self):
        return self._database_session.query(self._type).order_by(self._type.name).all()

    @dao_error_handler
    def create(self, data: dict):
        serie = Serie(name=data.get("name"), realisatorFirstName=data.get("realisatorFirstName"),
                      realisatorLastName=data.get("realisatorLastName"), likes=data.get("likes"),
                      genres=data.get("genres", []), startDate=data.get("startDate"),
                      numberOfSeasons=data.get("numberOfSeasons"), synopsis=data.get("synopsis"))
        self._database_session.add(serie)
        self._database_session.flush()
        return serie

    @dao_error_handler
    def update(self, serie: Serie, data: dict):
        super().update(serie, data)
        if 'startDate' in data:
            serie.startDate = data['startDate']
        if 'numberOfSeasons' in data:
            serie.numberOfSeasons = data['numberOfSeasons']
        self._database_session.merge(serie)
        self._database_session.flush()
        return serie

    @dao_error_handler
    def delete(self, entity):
        return super().delete(entity)
